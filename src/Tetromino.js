class Tetromino{
    constructor(color, shape) {
        this.color = color;
        this.shape = shape; // character
        this.members = [];
        this.setShape({row:0,col:7});
    }

    deplace(row,col = 0)
    {
        this.members.forEach( el => {
            el.row+= row;
            el.col+= col;
        });
    }

    setShape(center)
    {
        switch (this.shape) {
            case 'o':
                this.members.push(center);
                this.members.push({row: center.row + 1, col: center.col});
                this.members.push({row: center.row, col: center.col + 1});
                this.members.push({row: center.row + 1, col: center.col + 1});
                break;
            case 'i':
                this.members.push(center);
                this.members.push({row: center.row-1,col: center.col});
                this.members.push({row: center.row+1,col: center.col});
                this.members.push({row: center.row+2,col: center.col});
                break;
            case "z" :
                this.members.push(center);
                this.members.push({row: center.row,col: center.col+1});
                this.members.push({row: center.row-1,col: center.col});
                this.members.push({row: center.row-1,col: center.col-1});
                break;
            case "s" :
                this.members.push(center);
                this.members.push({row: center.row,col: center.col-1});
                this.members.push({row: center.row-1,col: center.col});
                this.members.push({row: center.row-1,col: center.col+1});
                break;
            case "j" :
                this.members.push(center);
                this.members.push({row: center.row-1,col: center.col});
                this.members.push({row: center.row-2,col: center.col});
                this.members.push({row: center.row,col: center.col-1});
                break;
            case "l" :
                this.members.push(center);
                this.members.push({row: center.row-1,col: center.col});
                this.members.push({row: center.row-2,col: center.col});
                this.members.push({row: center.row,col: center.col+1});
                break;
            case "t" :
                this.members.push(center);
                this.members.push({row: center.row,col: center.col-1});
                this.members.push({row: center.row,col: center.col+1});
                this.members.push({row: center.row+1,col: center.col});
                break;
        }
    }

    Rotation() {
        let newData = [];
        if(this.shape!=="o") {
            // Vérification du nombre d'éléments
            if (this.members.length < 4) {
                console.log("La pièce n'a pas suffisamment d'éléments pour être pivotée.");
                return;
            }

            // Point de pivot (premier élément)
            let pivot = this.members[0];

            // Angle de rotation en radians (90 degrés)
            let angle = Math.PI / 2;

            // Pour chaque élément sauf le pivot (premier élément)
            for (let i = 1; i < this.members.length; i++) {
                let currentElement = this.members[i];

                // Calcul des nouvelles coordonnées après la rotation
                let relativeX = currentElement.col - pivot.col;
                let relativeY = currentElement.row - pivot.row;

                let newX = Math.round(Math.cos(angle) * relativeX - Math.sin(angle) * relativeY + pivot.col);
                let newY = Math.round(Math.sin(angle) * relativeX + Math.cos(angle) * relativeY + pivot.row);
                newData.push({col:newX,row:newY})
            }

            for(let el of newData){
                if(!this.performRotation(el))
                {
                    return;
                }
            }
            for(let i = 1; i < this.members.length; i++) {
                this.members[i].row = newData[i-1].row;
                this.members[i].col = newData[i-1].col;
            }
        }

    }

    performRotation(newPosition){
        return (newPosition.row < 19 && newPosition.row >= 0 && newPosition.col < 15 && newPosition.col >= 0);
    }

}
export { Tetromino }