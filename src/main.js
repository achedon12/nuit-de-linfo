import {createApp} from 'vue'
import {createI18n} from "vue-i18n";
import './style.css'
import App from './App.vue'
import {createRouter, createWebHistory} from "vue-router";
import NotFound from './views/NotFound.vue'
import translations from './assets/translations.json'
import Cauchemar from './views/Cauchemar.vue'
import Particles from './components/travels/Particles.vue'
import Quizz from './views/Quizz.vue'
import Earth from "./components/travels/Earth.vue";
import About from "./views/About.vue";
import Contact from "./views/Contact.vue";
import ChatBox from './views/ChatBox.vue'

const routes = [
    {path: '/', component: Earth,},
    {path: '/cauchemar', name: 'UX/UI Cauchemardesque', component: Cauchemar},
    {path: '/particles', component: Particles},
    {path: '/quizz', name: 'Quizz', component: Quizz},
    {path: '/:pathMatch(.*)', name: 'NotFound', component: NotFound},
    {path: '/about', name: 'About', component: About},
    {path: '/contact', name: 'Contact', component: Contact},
    { path: '/chat-box', component: ChatBox },
    {path: '/earth', name: 'earth', component: Earth},
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

const i18n = createI18n({
    fallbackLocale: 'fr',
    messages: translations
})

export { i18n };

createApp(App).use(router).use(i18n).mount('#app')
